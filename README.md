The csv file provided in this repository contains the anonymized data that is used for the paper:

Jacob Krüger, Jens Wiemann, Wolfram Fenske, Gunter Saake, Thomas Leich: Do You Remember This Source Code?
In: Internationl Conference on Software Engineering (ICSE). ACM, 2018.

Details on how the data is obtained and what it describes can be found in this paper.

You are free to use the data for your own research, please cite the above paper if you do so.

Feel free to contact us if you have any questions.